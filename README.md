# flexibee-unpairer
## Odpárovávací script pro FlexiBee.
Script po spuštění provede odpárování vybraných bankovních dokladů ve FlexiBee.

## Instalace a použití
Do FlexiBee naimportujte uživatelské tlačítko, které je součástí repozitáře. Import provedete pomocí horního menu *Nástroje - Import - Import z XML*. Po úspěšném importu bude v evidenci Banka (horní menu *Peníze - Banka*) dostupné nové tlačítko Odpárovat.

Zaškrtejte doklady, které chcete odpárovat a klikněte na tlačítko Odpárovat.

Spustí se script, který po svém úspěšném dokončení vypíše kolik bylo odpárováno dokladů. Pokud dojde k chybě, bude vypsáno chybové hlášení a žádný doklad nebude odpárován. Opravte chybu a spusťte odpárování znovu.

## Systémové a licenční požadavky
* **REST-API pro zápis**. Pro správnou funkčnost uživatelského tlačítka je nutné mít některou z placených licencí FlexiBee. Tyto licence totiž obsahují REST-API s možností zápisu.

## Parametry volání scriptu
**url** - url encodovaná adresa flexibee serveru s idenitifkátorem firmy a evidencí (např. https://demo.flexibee.eu/c/demo/banka ).

**authSessionId** - authSessionId právě přihlášeného uživatele z FlexiBee. Odlášením uživatele z aplikace se zneplatní.

**ids** - seznam identifikátorů bankovních pohybů, které se mají odpárovat. Oddělené čárkou (např. 1,2,3). Povoleny jsou pouze interní identifikátory FlexiBee. 

## Zdroje
* https://charlieblog.eu/clanek-flexibee-uzivatelska-tlacitka
* https://www.flexibee.eu/api/dokumentace/ref/uzivatelske-tlacitko/
* https://www.flexibee.eu/api/dokumentace/ref/parovani-plateb/
* https://www.flexibee.eu/api/dokumentace/ref/login/
