<?php

function printUsage() {
    echo '<h1>Parametry URL:</h1>';
    echo '<p>url - url encodovaná adresa flexibee serveru s idenitifkátorem firmy a evidencí (např. https://demo.flexibee.eu/c/demo/banka ).</p>';
    echo '<p>authSessionId - authSessionId právě přihlášeného uživatele z FlexiBee.</p>';
    echo '<p>ids - seznam identifikátorů bankovních pohybů, které se mají odpárovat. Oddělené čárkou (např. 1,2,3).</p>';
}

$url = array_key_exists('url', $_GET) === TRUE ? $_GET['url'] : '';
$objectIdsStr = array_key_exists('ids', $_GET) === TRUE ? $_GET['ids'] : '';
$authSessionId = array_key_exists('authSessionId', $_GET) === TRUE ? $_GET['authSessionId'] : '';

if (empty($url) === TRUE || empty($authSessionId) === TRUE || empty($objectIdsStr) === TRUE) {
    printUsage();
    die();
}

$objectIds = explode(",", $objectIdsStr);

$xml = '<winstrom>';
foreach ($objectIds as $objectId) {
    $id = intval($objectId);
    if($id > 0) {
        $xml = $xml . '<banka><id>' . $id . '</id><odparovani></odparovani></banka>';
    }
}
$xml = $xml . '</winstrom>';

$finalUrl = urldecode($url) . '.xml';

// create curl resource
$ch = curl_init();

// return content as a string from curl_exec
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

// follow redirects (compatibility for changes in FlexiBee)
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

// HTTP authentication
curl_setopt($ch, CURLOPT_HTTPAUTH, FALSE);

// FlexiBee by default uses Self-Signed certificates
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

// for debugging
//curl_setopt($ch, CURLOPT_VERBOSE, TRUE);

// set username and password
//curl_setopt($ch, CURLOPT_USERPWD, "winstrom:winstrom");

// set URL
curl_setopt($ch, CURLOPT_URL, $finalUrl);

// set HTTP method
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

// set data
curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'X-authSessionId: ' . $authSessionId
    ));

curl_setopt($ch, CURLOPT_USERAGENT, 'FlexiBee-unpairer (more info at https://charlieblog.eu/clanek-flexibee-hromadny-odparovac)');

// execute
$output = curl_exec($ch);
$parsed = NULL;
if ($output === false) {
    echo 'Nepodařilo se připojit k serveru s FlexiBee.<br />';
    echo curl_error($ch) . '<br />';
    echo curl_errno($ch) . '<br />';
    die;
}

try {
    $parsed = new SimpleXMLElement($output);
} catch (Exception $e) {
    var_dump($e);
    echo '<br/><br/>';
}

if (is_null($parsed) === FALSE && ((string) $parsed->success) === 'true') {
    echo '<h1>Odpárování bylo úspěšné</h1>';
    echo '<p>Odpárováno bylo ' . $parsed->stats->updated . ' záznamů.</p>';
    echo '<p>Uzavřete okno a ve FlexiBee stiskněte tlačítko pro občerstvení dat (nebo klávesovou zkratku Ctrl + R).</p>';
} else {
    var_dump($parsed);
}
curl_close($ch);
